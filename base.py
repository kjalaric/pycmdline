'''
Script to convert a number from one base to another.
Maximum of 64. Representation is decimal numbers, then lowercase letters,
then uppercase letters, then "@!"

example use:
base.py 100 10 2 8 12 64
> converts 100 from base 10 into representations in base 2, 8, 12 and 64

kjalaric@gitlab
'''

import argparse
import string

CHAR_REPRESENTATION = list(string.digits + string.lowercase + string.uppercase + "@!")

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Script to convert a number from one base to another.")
    parser.add_argument('number', help="Number to convert.")
    parser.add_argument('original_base', help="Original base of number to convert.")
    parser.add_argument('new_bases', help="Base (or bases) to convert to.", nargs='*')
    args = parser.parse_args()
    
    decimal_representation = int(args.number, int(args.original_base))
    
    for base in args.new_bases:      
        if int(base) < 2 or int(base) > 64:
            print("Cannot convert to base {}.".format(base))
            continue
        mods = list()
        num = decimal_representation
        while num > 0:
            mods.append(CHAR_REPRESENTATION[num % int(base)])
            num = num // int(base)
        print(base + ":" + "".join(reversed(mods)))        